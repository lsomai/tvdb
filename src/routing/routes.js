import Home from '../pages/Home';

import SearchResults from '../pages/SearchResults';
import { ErrorComponent } from '../shared-components';
import Detail from '../pages/Detail/Detail';

const ROUTES = [
    {
        path: '/',
        component: Home,
        exact: true,
    },
    {
        path: '/tvshow/:id',
        component: Detail,
    },
    {
        path: '/search-results/:term',
        component: SearchResults,
    },
    {
        path: '*',
        component: ErrorComponent,
    },
];

export default ROUTES;
