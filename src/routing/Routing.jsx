import React from 'react';
import { Switch, Route } from 'react-router-dom';
import ROUTES from './routes';

function Routing() {
    return (
        <Switch>
            {ROUTES.map((route) => (
                <Route exact={route.exact} key={route.path} path={route.path} render={() => <route.component />} />
            ))}
        </Switch>
    );
}

export default Routing;
