import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Search from '../components/search/Search';
import useGetMovies from '../hooks/useGetMovies';
import { LoadingComponent, ErrorComponent } from '../shared-components';
import Movies from '../components/movies/Movies';
import PageTitle from '../components/page-title/PageTitle';

function Home() {
    const movies = useGetMovies();

    if (movies.isLoading) {
        return <LoadingComponent />;
    }

    if (movies.hasError) {
        return <ErrorComponent />;
    }

    return (
        <>
            <Search />
            <PageTitle pageTitle="Most popular" />
            <Container fluid className="home-page wrapper">
                <Container>
                    <Row>
                        <Movies movies={movies.data} />
                    </Row>
                </Container>
            </Container>
        </>
    );
}

export default Home;
