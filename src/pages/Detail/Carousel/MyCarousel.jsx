import React from 'react';
import Carousel from 'react-bootstrap/Carousel';

function MyCarousel({ pictures }) {
    if (!pictures || pictures.length === 0) {
        return null;
    }
    return (
        <>
            <Carousel>
                {pictures.map((picture) => (
                    <Carousel.Item key={picture}>
                        <img className="d-block w-100" src={picture} alt="-" />
                    </Carousel.Item>
                ))}
            </Carousel>
        </>
    );
}

export default MyCarousel;
