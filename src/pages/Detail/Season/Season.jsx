import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Episode from '../Episode/Episode';

function Season({ episodes, openEpisodeModal }) {
    return (
        <ListGroup variant="flush" as="div">
            {episodes.map((episode) => (
                <ListGroup.Item action key={Math.random() + 10000}>
                    <Episode episode={episode} openEpisodeModal={openEpisodeModal} />
                </ListGroup.Item>
            ))}
        </ListGroup>
    );
}

export default Season;
