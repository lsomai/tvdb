import React from 'react';
import Badge from 'react-bootstrap/Badge';

function Genre({ genres }) {
    return genres.map((genre) => (
        <Badge pill variant="secondary" className="ml-1" key={genre}>
            {genre}
        </Badge>
    ));
}

export default Genre;
