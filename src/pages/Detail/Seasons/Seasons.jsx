import React, { useState } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import groupBySeason from '../../../utils/groupBySeason';
import Season from '../Season/Season';
import EpisodeDetails from '../Episode/EpisodeDetails/EpisodeDetails';

function Seasons({ episodes }) {
    const [modal, setModal] = useState({ isOpen: false, episode: {} });

    const openEpisodeModal = (episode) => {
        setModal({ isOpen: true, episode });
    };

    const closeModal = () => {
        setModal({ isOpen: false, episode: {} });
    };

    const groupedEpisodes = groupBySeason(episodes);
    const seasons = Object.keys(groupedEpisodes);

    return (
        <>
            <p className="h4 mb-3">Seasons</p>
            <Accordion defaultActiveKey="0">
                {seasons.map((season) => {
                    return (
                        // Using UUID lib in future
                        <Card key={Math.random() + 1000}>
                            <Card.Header>
                                <Accordion.Toggle as={Button} variant="link" eventKey={season}>
                                    <p className="font-weight-bold mb-0">
                                        <span className="mr-1">Season</span>
                                        {season}
                                    </p>
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey={season}>
                                <Card.Body>
                                    <Season episodes={groupedEpisodes[season]} openEpisodeModal={openEpisodeModal} />
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    );
                })}
            </Accordion>
            <EpisodeDetails modal={modal} closeModal={closeModal} />
        </>
    );
}

export default Seasons;
