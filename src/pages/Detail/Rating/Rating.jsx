import React from 'react';
import Form from 'react-bootstrap/Form';

function Rating({ rating, totalRatings }) {
    // Episodate API is using 10 star rating, displayed using number value, not stars.
    // I divided the rating value by 2, and rounded it to get a normalized value.
    // For human eyes. are easier to recognize a value from 5 stars (personal opinion).
    let normalizedRating = 0;

    if (rating) {
        normalizedRating = Math.round(rating / 2);
    }

    if (!totalRatings) {
        totalRatings = 0;
    }

    return (
        <div className="rating d-flex justify-content-end">
            {[...Array(5)].map((star, i) => {
                const ratingValue = i + 1;

                return (
                    <span className="mr-1" key={ratingValue}>
                        <Form.Check type="radio" value={ratingValue} className="d-none" />
                        <svg
                            width="1.2em"
                            height="1.2em"
                            viewBox="0 0 16 16"
                            className="bi bi-star-fill"
                            fill={ratingValue <= normalizedRating ? '#f2dc5d' : 'grey'}
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.283.95l-3.523 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                        </svg>
                    </span>
                );
            })}
            <span>{` / ${totalRatings}`}</span>
        </div>
    );
}

export default Rating;
