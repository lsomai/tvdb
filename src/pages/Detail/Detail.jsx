import React from 'react';
import { useParams } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import useGetMovie from '../../hooks/useGetMovie';
import { LoadingComponent, ErrorComponent } from '../../shared-components';
import Search from '../../components/search/Search';
import PageTitle from '../../components/page-title/PageTitle';
import Genre from './Genre/Genre';
import Rating from './Rating/Rating';
import MyCarousel from './Carousel/MyCarousel';
import Seasons from './Seasons/Seasons';

function Detail() {
    const { id } = useParams();

    const movie = useGetMovie(id);

    if (movie.isLoading) {
        return <LoadingComponent />;
    }

    if (movie.hasError) {
        return <ErrorComponent />;
    }
    return (
        <>
            <Search />
            <PageTitle pageTitle="TV Show" />
            <Container fluid className="search-results-page wrapper">
                <Container>
                    <Row>
                        <Col lg={4}>
                            <Image src={movie.data.image_path} thumbnail />
                        </Col>
                        <Col lg={8} className="bg-white rounded border">
                            <div className="px-2 py-3">
                                <div className="d-flex align-items-center justify-content-between border-bottom pb-2">
                                    <h1 className="h3 mb-0">{movie.data.name}</h1>
                                    <div>
                                        <Genre genres={movie.data.genres} />
                                    </div>
                                </div>
                                <p className="mt-3">
                                    <span className="mr-3">
                                        <strong>Start: </strong>
                                        {movie.data.start_date ? movie.data.start_date : '-'}
                                    </span>
                                    <span>
                                        <strong>Status: </strong>
                                        {movie.data.status ? movie.data.status : '-'}
                                    </span>
                                </p>
                                <div>
                                    <strong>Description:</strong>
                                    <p className="mt-1 text-justify">
                                        <span>{movie.data.description}</span>
                                    </p>
                                </div>
                                <Rating rating={movie.data.rating} totalRatings={movie.data.rating_count} />
                                <div className="mt-3">
                                    <MyCarousel pictures={movie.data.pictures} />
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row className="mt-4">
                        <Col>
                            <Seasons episodes={movie.data.episodes} />
                        </Col>
                    </Row>
                </Container>
            </Container>
        </>
    );
}

export default Detail;
