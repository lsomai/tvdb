import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

function EpisodeDetails({ modal, closeModal }) {
    return (
        <Modal show={modal.isOpen}>
            <Modal.Header>
                <Modal.Title>
                    <span className="font-weight-bold mr-2">{`E${modal.episode.episode}:`}</span>
                    {modal.episode.name}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>
                    <span className="mr-2">
                        <strong>Director: </strong>
                        N/A
                    </span>
                    <span className="mr-2">
                        <strong>Overview: </strong>
                        N/A
                    </span>
                    <span>
                        <strong>Image: </strong>
                        N/A
                    </span>
                </p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={closeModal}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default EpisodeDetails;
