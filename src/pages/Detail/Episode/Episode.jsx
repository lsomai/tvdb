import React from 'react';
import Button from 'react-bootstrap/Button';

function Episode({ episode, openEpisodeModal }) {
    return (
        <div className="d-flex justify-content-between align-items-center">
            <span className="font-weight-bold mr-2">{`E${episode.episode}:`}</span>
            <span className="mr-3 font-italic">{episode.name}</span>
            <span>
                <strong>Air date: </strong>
                {episode.air_date}
            </span>
            <Button variant="outline-secondary" onClick={() => openEpisodeModal(episode)}>
                More details
            </Button>
        </div>
    );
}

export default Episode;
