import React from 'react';
import { useParams } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Search from '../components/search/Search';
import useSearchMovies from '../hooks/useSearchMovies';
import { LoadingComponent, ErrorComponent } from '../shared-components';
import Movies from '../components/movies/Movies';
import PageTitle from '../components/page-title/PageTitle';

function SearchResults() {
    const { term } = useParams();
    const movies = useSearchMovies(term);

    if (movies.isLoading) {
        return <LoadingComponent />;
    }

    if (movies.hasError) {
        return <ErrorComponent />;
    }

    return (
        <>
            <Search />
            <PageTitle pageTitle={`Search results for: ${term}`} />
            <Container fluid className="search-results-page wrapper">
                <Container>
                    <Row>
                        <Movies movies={movies.data} />
                    </Row>
                </Container>
            </Container>
        </>
    );
}

export default SearchResults;
