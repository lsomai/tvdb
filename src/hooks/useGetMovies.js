import { useEffect, useState } from 'react';
import axios from 'axios';

function useGetMovies(page = 1) {
    const [isLoading, setLoading] = useState(true);
    const [hasError, setError] = useState(false);
    const [data, setData] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const response = await axios.get(`/most-popular?page=${page}`);
                setData(response.data.tv_shows);
            } catch (error) {
                setError(true);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [page]);

    return { data, isLoading, hasError };
}

export default useGetMovies;
