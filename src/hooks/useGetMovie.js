import { useEffect, useState } from 'react';
import axios from 'axios';

function useGetMovie(movieId = 1) {
    const [isLoading, setLoading] = useState(true);
    const [hasError, setError] = useState(false);
    const [data, setData] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            try {
                const response = await axios.get(`/show-details?q=${movieId}`);
                setData(response.data.tvShow);
            } catch (error) {
                setError(true);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, [movieId]);

    return { data, isLoading, hasError };
}

export default useGetMovie;
