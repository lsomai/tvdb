import React from 'react';
import './App.scss';
import { BrowserRouter } from 'react-router-dom';
import Routing from './routing/Routing';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Header />
                <Routing />
                <Footer />
            </BrowserRouter>
        </div>
    );
}

export default App;
