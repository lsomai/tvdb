import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

function Movie({ movie }) {
    return (
        <Card className="">
            <Card.Img variant="top" src={movie.image_thumbnail_path} className="movie-card-img" />
            <Card.Title className="bg-primary p-2 text-center text-light">{movie.name}</Card.Title>
            <Card.Body className="py-0">
                <p className="start-date mb-0">
                    <strong>Start: </strong>
                    {movie.start_date ? movie.start_date : '-'}
                </p>
                <p className="status">
                    <strong>Status: </strong>
                    {movie.status ? movie.status : '-'}
                </p>
                <p className="network">
                    <strong>Network: </strong>
                    {movie.network ? movie.network : '-'}
                </p>
                <Link to={`/tvshow/${movie.id}`} className="stretched-link text-decoration-none" />
            </Card.Body>
        </Card>
    );
}

export default Movie;
