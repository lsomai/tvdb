import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Footer() {
    return (
        <Container fluid className="bg-dark">
            <Container>
                <Row>
                    <Col>
                        <p className="text-small text-center text-light mb-0 py-2">
                            Copyright &copy; TV SHOW DATABASE 2020
                        </p>
                    </Col>
                </Row>
            </Container>
        </Container>
    );
}

export default Footer;
