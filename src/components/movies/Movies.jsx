import React from 'react';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import Movie from '../movie/Movie';

function Movies({ movies }) {
    if (!movies || movies.length === 0) {
        return (
            <Col>
                <Alert variant="info">
                    <p className="mb-0 text-center">No results :(</p>
                </Alert>
            </Col>
        );
    }

    return (
        <>
            {movies.map((movie) => (
                <Col lg={3} className="mb-3">
                    <Movie movie={movie} />
                </Col>
            ))}
        </>
    );
}

export default Movies;
