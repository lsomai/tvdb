import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import { Link } from 'react-router-dom';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';

function Search() {
    const [searchTerm, setSearchTerm] = useState('');

    return (
        <Container>
            <Row>
                <Col className="my-5">
                    <Form>
                        <Form.Group controlId="searchForm">
                            <Form.Label column className="text-center mb-3">
                                <p className="h3">
                                    <svg
                                        width="1em"
                                        height="1em"
                                        viewBox="0 0 16 16"
                                        className="bi bi-search mr-3"
                                        fill="currentColor"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"
                                        />
                                        <path
                                            fillRule="evenodd"
                                            d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"
                                        />
                                    </svg>
                                    Do you have a favorite TV show? Find it.
                                </p>
                            </Form.Label>
                            <Form.Control
                                type="text"
                                className="text-center"
                                placeholder="What's your favorite TV Show`s name?"
                                onInput={(event) => setSearchTerm(event.target.value)}
                            />
                        </Form.Group>
                        <Form.Group>
                            <Button variant="danger" className="mx-auto d-block w-25" disabled={searchTerm === ''}>
                                <Link to={`/search-results/${searchTerm}`} className="text-white text-decoration-none">
                                    Search
                                </Link>
                            </Button>
                        </Form.Group>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

export default Search;
