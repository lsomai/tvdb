import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function PageTitle({ pageTitle = '' }) {
    return (
        <Container>
            <Row>
                <Col className="page-title py-2 rounded-top">
                    <p className="h6 text-center mb-0">{pageTitle}</p>
                </Col>
            </Row>
        </Container>
    );
}

export default PageTitle;
